import XCTest

#if !canImport(ObjectiveC)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(oz_liveness_iosTests.allTests),
    ]
}
#endif
