import XCTest

import oz_liveness_iosTests

var tests = [XCTestCaseEntry]()
tests += oz_liveness_iosTests.allTests()
XCTMain(tests)
