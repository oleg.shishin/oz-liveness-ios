// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "oz-liveness-ios",
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "OZLivenessSDK",
            targets: ["OZLivenessSDK"]),
    ],
    
    dependencies: [
        
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
        .package(url: "https://gitlab.com/oleg.shishin/tensorflowlitec.git",from: "2.2.5")
    ],
    
    targets: [
        .binaryTarget(
            name: "OZLivenessSDK",
            path: "OZLivenessSDK.xcframework")
    ]
)
